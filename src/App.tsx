import React, { FC, ReactElement } from "react";
import { Provider } from "react-redux";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { store } from "./store/store";
import CommentsSection from "./containers/CommentsSection/CommentsSection.container";

const App: FC = (): ReactElement => {
    return (
        <Provider store={store}>
          <div>
              <CommentsSection />
              <ToastContainer />
          </div>
        </Provider>
    );
};

export default App;
