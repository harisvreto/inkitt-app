import React, { ReactElement, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import moment from "moment";
import { faker } from "@faker-js/faker";
import { toast } from "react-toastify";

import { addComment } from "../../store/comments/comments.actions";
import { Comment } from "../../store/comments/comments.types";
import CommentInput from "../../components/CommentInput/CommentInput.component";
import CommentsList from "../../components/CommentsList/CommentsList.component";
import { selectGroupedComments } from "../../store/comments/comments.selectors";

const CommentsSection: React.FC = (): ReactElement => {
    const dispatch = useDispatch();

    const [message, setMessage] = useState<string>("");
    const [replyCommentId, setReplyCommentId] = useState<string>("");

    const comments = useSelector(selectGroupedComments);

    const _addComment = (comment_message: string) => {
        if (!comment_message) {
            toast.error("Comment cannot be empty.");
            return;
        }

        const new_comment: Comment = {
            id: uuidv4(),
            parent_id: "root",
            message: comment_message,
            up_votes: 0,
            down_votes: 0,
            date_created: moment().valueOf(),
            date_updated: null,
            user: {
                id: uuidv4(),
                image: faker.image.image(36, 36, true),
                username: faker.name.findName(),
            },
        };

        dispatch(addComment(new_comment));
        setMessage("");
    };

    const _replyComment = (reply_message: string, parent_id: string) => {
        if (!reply_message) {
            toast.error("Replay cannot be empty.");
            return;
        }

        const new_comment: Comment = {
            id: uuidv4(),
            parent_id,
            message: reply_message,
            up_votes: 0,
            down_votes: 0,
            date_created: moment().valueOf(),
            date_updated: null,
            user: {
                id: uuidv4(),
                image: faker.image.image(36, 36, true),
                username: faker.name.findName(),
            },
        };

        dispatch(addComment(new_comment));
        setReplyCommentId("");
    };

    return (
        <section className="comments-section p-10">
            <CommentInput
                message={message}
                setMessage={setMessage}
                addComment={_addComment}
            />
            <CommentsList
                comments={comments}
                replyComment={_replyComment}
                replyCommentId={replyCommentId}
                setReplyCommentId={setReplyCommentId}
            />
        </section>
    );
};

export default CommentsSection;
