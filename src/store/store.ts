import { createStore, applyMiddleware } from 'redux';
import rootReducer from './rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';

export const store = createStore(
  rootReducer(),
  process.env.NODE_ENV !== 'production'
    ? composeWithDevTools()
    : applyMiddleware(),
);
