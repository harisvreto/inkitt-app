import { createSelector } from "reselect";
import { Comment } from "./comments.types";

const selectComments = (state: any) => state.comments.list;
const selectCommentsMap = createSelector(
  [selectComments],
  (comments) =>
    comments.reduce(
      (comments: any, comment: any) =>
        comments.set(comment.id, comment),
      new Map()
    )
);

const recursiveUpdate = (updated: Comment, nestedMap: any) => {
    const recursion: Function = (updated: Comment) => {
        nestedMap.set(updated.id, updated);
        if (updated.id === "root") {
            return;
        }
        const parent = nestedMap.get(updated.parent_id);
        const newParent = {
            ...parent,
            child_comments: parent.child_comments.map((child: Comment) =>
                child.id === updated.id ? updated : child
            ),
        };
        return recursion(newParent);
    };
    return recursion(updated, nestedMap);
};

const addNewComment = (comment: Comment, nestedMap: any) => {
    comment = { ...comment, child_comments: [] };

    nestedMap.set(comment.id, comment);
    const parent = nestedMap.get(comment.parent_id);
    const updatedParent = {
        ...parent,
        child_comments: [comment, ...parent.child_comments],
    };

    recursiveUpdate(updatedParent, nestedMap);
};

export const selectGroupedComments = (() => {
    const nestedMap: any = new Map([["root", { id: "root", child_comments: [] }]]);

    return createSelector([selectCommentsMap], (currentMap) => {
        [...currentMap.entries()].forEach(([id, comment]) => {
            if (!nestedMap.get(id)) {
                addNewComment(comment, nestedMap);
            }
        });

        return nestedMap.get("root").child_comments;
    });
})();
