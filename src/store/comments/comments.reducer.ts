import {
    CommentsList,
    ADD_COMMENT,
} from "./comments.types";

const INITIAL_STATE: CommentsList = {
    list: [],
};

const comments = (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case ADD_COMMENT:
            return {
                ...state,
                list: [...state.list, action.comment],
            };
        default:
            return state;
    }
};

export default comments;
