export type Comment = {
    id: string;
    parent_id: string | null;
    message: string;
    up_votes: number;
    down_votes: number;
    date_created: number;
    date_updated: number | null;
    user: User;
    child_comments?: any[];
};

type User = {
    id: string;
    image: string;
    username: string;
};

export type CommentsList = {
    list: Comment[];
};

export const ADD_COMMENT = 'ADD_COMMENT';