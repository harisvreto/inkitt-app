import { Comment, ADD_COMMENT } from "./comments.types";

export const addComment = (comment: Comment) => {
    return { type: ADD_COMMENT, comment };
};

