import { combineReducers, CombinedState, Reducer } from 'redux';
import comments from './comments/comments.reducer';

const rootReducer = (): Reducer<CombinedState<any>, any> =>
  combineReducers({
    comments: comments,
  });

export default rootReducer;
