import { Comment } from "../../store/comments/comments.types";

export default interface ICommentItem {
    comment: Comment;
    replyCommentId: string;
    setReplyCommentId: Function;
    replyComment: Function;
}