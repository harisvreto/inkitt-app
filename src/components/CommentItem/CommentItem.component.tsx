import {
    faCommentAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import React, { memo, ReactElement, useState } from "react";
import { Comment } from "../../store/comments/comments.types";
import CommentInput from "../CommentInput/CommentInput.component";

import ICommentItem from "./CommentItem.interface";

const CommentItem: React.FC<ICommentItem> = memo(
    ({
        comment,
        replyCommentId,
        setReplyCommentId,
        replyComment,
    }: ICommentItem): ReactElement => {
        const isReplayActive = comment.id === replyCommentId;

        const [message, setMessage] = useState<string>("");

        return (
            <div className="comments-together pl-[36px]">
                <li className="comment-item">
                    <div className="comment-header flex items-center ml-[-36px]">
                        <img
                            src={comment.user.image}
                            alt={comment.user.username}
                            className="w-36px h-36px rounded-full"
                        />
                        <div className="flex flex-col ml-[10px]">
                            <p className="text-sm font-medium">
                                {comment.user.username}
                            </p>
                            <p className="text-xs">
                                {moment(comment.date_created).fromNow()}
                            </p>
                        </div>
                    </div>
                    <div className="pt-[10px] pl-[10px]">
                        <p className="pb-[5px]">{comment.message}</p>
                        <div className="pb-[10px]">
                            <div className="flex items-center">
                                <button
                                    className="bg-gray-200 hover:bg-gray-300 text-gray-800 text-sm font-bold py-1 px-2 rounded inline-flex items-center"
                                    onClick={() => {
                                        setReplyCommentId(
                                            isReplayActive ? "" : comment.id
                                        );
                                        setMessage("");
                                    }}
                                >
                                    <FontAwesomeIcon icon={faCommentAlt} />
                                    <span className="ml-1 text-xs">Replay</span>
                                </button>
                            </div>
                            {isReplayActive && (
                                <div className="p-3 mt-2 bg-gray-100 shadow-lg rounded">
                                    <CommentInput
                                        message={message}
                                        setMessage={setMessage}
                                        addComment={replyComment}
                                        parentId={comment.id}
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                </li>
                <ul className="child-comments">
                    {comment.child_comments &&
                        comment.child_comments.map(
                            (child_comments_comment: Comment) => (
                                <CommentItem
                                    key={child_comments_comment.id}
                                    comment={child_comments_comment}
                                    replyCommentId={replyCommentId}
                                    setReplyCommentId={setReplyCommentId}
                                    replyComment={replyComment}
                                />
                            )
                        )}
                </ul>
            </div>
        );
    }
);

export default CommentItem;
