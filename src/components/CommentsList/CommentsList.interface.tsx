import { Comment } from "../../store/comments/comments.types";

export default interface ICommentsList {
    comments: Comment[];
    replyCommentId: string;
    setReplyCommentId: Function;
    replyComment: Function;
}