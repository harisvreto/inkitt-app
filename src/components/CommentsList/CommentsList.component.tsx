import React, { memo, ReactElement } from "react";
import { Comment } from "../../store/comments/comments.types";
import CommentItem from "../CommentItem/CommentItem.component";

import ICommentsList from "./CommentsList.interface";

const CommentsList: React.FC<ICommentsList> = memo(
    ({
        comments,
        replyCommentId,
        setReplyCommentId,
        replyComment,
    }: ICommentsList): ReactElement => {
        return (
            <div>
                {comments.length > 0 && (
                    <ul className="comments-list mt-5">
                        {comments.map((comment: Comment) => (
                            <CommentItem
                                key={comment.id}
                                comment={comment}
                                replyCommentId={replyCommentId}
                                setReplyCommentId={setReplyCommentId}
                                replyComment={replyComment}
                            />
                        ))}
                    </ul>
                )}
                {comments.length <= 0 && (
                    <p className="text-center p-10">No comments found.</p>
                )}
            </div>
        );
    }
);

export default CommentsList;
