export default interface ICommentInput {
    message: string;
    setMessage: Function;
    addComment: Function;
    parentId?: string;
}